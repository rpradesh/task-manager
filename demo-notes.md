* Note:
    - I am thinking of using first half hour to show and describe the application and its implementation.    
    - Please interrupt me for any questions you have in the presentation as we flow through
    - But for dev/technical discussion of why/how any, I am keeping the rest half-hour assigned for it. 
    
### Application Components
* Actor: User
* Use Case: Manage (His) Tasks
* Subject: User, Task; 
* Relationship: A user has 0 or more tasks

### About the Application
* What: 
    - Manage Tasks for an User
* Why: 
    - Check with NodeJS and SDLC experience/time :)
    - Use case - simpler like API+Persistence
    - Mostly not for co-ordinator/multi-threaded apps

### How - Implementation of Components
* REST for CRUD of Users/Tasks
* ExpressJS provides 
    - MVC/REST framework
    - Middleware (Intercept/Filter) 
* Mongoose/MongoDB
    - Validation, Transform, Lazy/Eager Join.,

* Note: 
    - This application does Not include distributed components  like - Messaging/Consuming services, to keep it simple     
    - This demo is not about to show, all bells/whistles being built.
    - But instead to show how common features of data validation/persistence/intecept/authen and servicing REST api implemented at right place of library/api call
    - Without repeating the logic and is easy to extend.

### General Application Features
* Token based authentication
* File Upload - binary type in mongo
* Users can add/edit/delete their own tasks
* DAO
    * Query with pagination and filter
    * Lazy/Eager join Query  
    * Insert/Update with allowed fields only
    * Cascade Delete - delete of User, deletes his tasks
    * Validation in Mongo Model
* Integration test - API test
* Developer Friendly-ness !?
    * nodemon/Jest has --watch options        

## Application Demo 
- I will not be going though all the REST endpoints in demo but here is the list to skim through.
- But instead highlighting the feature with right use of library/api 
- You might notice the "model" package here does more than entity Model, it has intercept+validation etc., 
       
* Create/Login User -  CreateUser;Get User/me;LogOut-Once;LogOut-Twice-see 200 changes to 401
    - Show routers/user.js
* Create User2 - trim Password to 4 and show error Msg
    - Notice Validation of Password msg details useful for UI
    - show Schema.User model
        * Show how "pre" hook of password update is encrypted
        * Validation 
            - How email pattern is validated
        * Transform/Hide Fields 
            - no password is in GET-User/ID
            - email is made into lowercase and trim 
        * Index
            - How Unique and Natural index are declarative                    
* "Create 2 Tasks under user 1"; and list Tasks   
        * Show List User and SortBy CreatedAt:desc            
* Delete User
    * Show how "pre" hook does cascade-delete of tasks by Delete User 
    * Delete Twice, ofcourse user is gone, session is also gone       
* LAST show Running Jest - it does Real Integration Test not like JUnit or Mock
    * Advantage of JEST is without launching real listener running    

### Auth Interceptor
* Owner of Task can only do CRUD his tasks -  Middleware (Intercept/Filter)
* Used with Express, how auth is intercepted function by passing "auth"
 - for all functions, except Create/Login
 - show sa-feeds server for app.use of registering Handlers


#### User Routes
* router.post('/users', async (req, res) => {
* router.post('/users/login', async (req, res) => {
* router.post('/users/logout', auth, async (req, res) => {
* router.post('/users/logoutAll', auth, async (req, res) => { //many sessions alive

#### using "/me" instead of passing "_id" of logged in user in REST URIs
* router.get('/users/me', auth, async (req, res) => {
* router.patch('/users/me', auth, async (req, res) => {
* router.delete('/users/me', auth, async (req, res) => {

#### Test fo File GET/Upload
* router.post('/users/me/avatar', auth, upload.single('avatar'), async (req, res) => {
* router.delete('/users/me/avatar', auth, async (req, res) => {
* router.get('/users/:id/avatar', async (req, res) => {

#### Task CRUD Routes - note this api considers owner of Task as implicit filter (Security/Authorized)
* router.post('/tasks', auth, async (req, res) => {
* router.get('/tasks', auth, async (req, res) => {
* router.get('/tasks/:id', auth, async (req, res) => {
* router.patch('/tasks/:id', auth, async (req, res) => {
* router.delete('/tasks/:id', auth, async (req, res) => {
