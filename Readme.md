## Node Application - Task Manager  Application with Integration Testing
```bash
# start local mongo
docker ps -a
docker start localMongod41
```
 
* Build and start app
```bash 
nvm install 12.3.1
node -v

cd ~/projects/task-manager
#nvm use
npm install

# runs under watch !
npm run dev
```

## RevealJS
* Steps at https://github.com/hakimel/reveal.js#full-setup
* See demo working on net http://revealjs.com/
* Follow for example slides code https://github.com/hakimel/reveal.js/blob/master/demo.html
```bash
nvm install 10.15.3
nvm use 10.15.3
node -v

cd ~/projects/task-manager/demo
git clone --depth=1 https://github.com/hakimel/reveal.js.git
cd reveal.js
rm -rf ./.git
npm install
npm start -- --port=8002

# browse at http://localhost:8002/
# <!-- .element: class="fragment" -->
```