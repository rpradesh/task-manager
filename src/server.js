process.env['MONGODB_URL']='mongodb://127.0.0.1:27017/task-manager-api'
process.env['JWT_SECRET']='thisisasecretformyapp'

const app = require('./app')
const port = process.env.PORT || 3000

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})