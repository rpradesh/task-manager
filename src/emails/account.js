const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    /*
    sgMail.send({
        to: email,
        from: 'rao.rajesh8@gmail.com',
        subject: 'Thanks for joining in!',
        text: `Welcome to the app, ${name}. Let me know how you get along with the app.`
    })
    */
    console.log(`Sending "Welcome Email" to email=${email} name=${name}`)
}

const sendCancelationEmail = (email, name) => {
    /*
    sgMail.send({
        to: email,
        from: 'rao.rajesh8@gmail.com',
        subject: 'Sorry to see you go!',
        text: `Goodbye, ${name}. I hope to see you back sometime soon.`
    })
    */
    console.log(`Sending "Cancel Email" to email=${email} name=${name}`)
}

module.exports = {
    sendWelcomeEmail,
    sendCancelationEmail
}